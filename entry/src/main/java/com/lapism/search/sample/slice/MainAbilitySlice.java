package com.lapism.search.sample.slice;

import com.lapism.search.internal.SearchLayout;
import com.lapism.search.sample.ResourceTable;
import com.lapism.search.widget.MaterialSearchView;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.InputAttribute;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MainAbilitySlice extends AbilitySlice {
    private class MyItemProvider extends BaseItemProvider {
        private int keyLength = 0;
        List<String> allData = Arrays.asList(new String[]{"lightpink", "pink", "crimson", "lavenderblush",
                "palevioletred", "hotpink", "deeppink", "mediumvioletred", "orchid", "thistle", "plum", "violet",
                "magenta", "fuchsia", "darkmagenta", "purple", "mediumorchid", "darkviolet", "darkorchid", "indigo",
                "blueviolet", "mediumpurple", "mediumslateblue", "slateblue", "darkslateblue", "lavender",
                "ghostwhite", "blue", "mediumblue", "midnightblue", "darkblue", "navy", "royalblue", "cornflowerblue"
                , "lightsteelblue", "lightslategray", "slategray", "dodgerblue", "aliceblue", "steelblue",
                "lightskyblue", "skyblue", "deepskyblue", "lightblue", "powderblue", "cadetblue", "azure", "lightcyan"
                , "paleturquoise", "cyan", "aqua", "darkturquoise", "darkslategray", "darkcyan", "teal",
                "mediumturquoise", "lightseagreen", "turquoise", "aquamarine", "mediumaquamarine", "mediumspringgreen"
                , "mintcream", "springgreen", "mediumseagreen", "seagreen", "honeydew", "lightgreen", "palegreen",
                "darkseagreen", "limegreen", "lime", "forestgreen", "green", "darkgreen", "chartreuse", "lawngreen",
                "greenyellow", "darkolivegreen", "yellowgreen", "olivedrab", "beige", "lightgoldenrodyellow", "ivory"
                , "lightyellow", "yellow", "olive", "darkkhaki", "lemonchiffon", "palegoldenrod", "khaki", "gold",
                "cornsilk", "goldenrod", "darkgoldenrod", "floralwhite", "oldlace", "wheat", "moccasin", "orange",
                "papayawhip", "blanchedalmond", "navajowhite", "antiquewhite", "tan", "burlywood", "bisque",
                "darkorange", "linen", "peru", "peachpuff", "sandybrown", "chocolate", "saddlebrown", "seashell",
                "sienna", "lightsalmon", "coral", "orangered", "darksalmon", "tomato", "mistyrose", "salmon", "snow",
                "lightcoral", "rosybrown", "indianred", "red", "brown", "firebrick", "darkred", "maroon", "white",
                "whitesmoke", "gainsboro", "lightgrey", "silver", "darkgray", "gray", "dimgray", "black"});
        {
            allData.sort(Comparator.naturalOrder());
        }

        public void filter(String key) {
            keyLength = key.length();
            String lowerKey = key.toLowerCase();
            data.clear();
            if (keyLength > 0) {
                data.addAll(allData.stream().filter(s -> s.startsWith(lowerKey)).collect(Collectors.toList()));
            }
            notifyDataChanged();
        }

        private List<String> data = new ArrayList<>();

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            if (component == null) {
                component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_list_item, null, false);
            }
            String text = data.get(i);
            RichText richText =
                    new RichTextBuilder()
                            .mergeForm(new TextForm().setRelativeTextSize(1.2f).setTextColor(0xffff0000).setTextFont(Font.DEFAULT_BOLD))
                            .addText(text.substring(0, keyLength)).revertForm().addText(text.substring(keyLength)).build();
            ((Text) component.findComponentById(ResourceTable.Id_txt)).setRichText(richText);
            return component;
        }
    }

    private MaterialSearchView materialSearchView;
    private RadioContainer navigationIconSelector;
    private RadioContainer layoutManagerSelector;
    private RadioContainer inputTypeSelector;
    private RadioContainer imeOptionSelector;
    private RadioContainer textColorSelector;
    private RadioContainer textSizeSelector;
    private RadioContainer hintSelector;
    private RadioContainer hintColorSelector;
    private RadioContainer elevationSelector;
    private RadioContainer strokeWidthSelector;
    private RadioContainer strokeColorSelector;
    private RadioContainer radiusSelector;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        MyItemProvider adapter = new MyItemProvider();


        materialSearchView = (MaterialSearchView) findComponentById(ResourceTable.Id_search1);
        materialSearchView.setAdapter(adapter);

        materialSearchView.setNavigationIconSupport(SearchLayout.NavigationIconSupport.SEARCH);
        materialSearchView.setOnNavigationClickListener(new SearchLayout.OnNavigationClickListener() {
            @Override
            public void onNavigationClick(boolean hasFocus) {
                if (materialSearchView.hasFocus()) {
                    materialSearchView.clearFocus();
                } else {
                    materialSearchView.requestFocus();
                }
            }
        });

        materialSearchView.setTextHint("Search");
        materialSearchView.setOnQueryTextListener(new SearchLayout.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                materialSearchView.clearFocus();
                showSearchResultDialog(query);
                return true;
            }
        });

        materialSearchView.setOnMicClickListener(new SearchLayout.OnMicClickListener() {
            @Override
            public void onMicClick() {

            }
        });

        materialSearchView.setBackgroundRadius(15);
        materialSearchView.setBackgroundStrokeWidth(AttrHelper.vp2px(1,getContext()));
        materialSearchView.setBackgroundStrokeColor(0xff6200ee);
        materialSearchView.setOnFocusChangeListener(new SearchLayout.OnFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                materialSearchView.setNavigationIconSupport(hasFocus ? SearchLayout.NavigationIconSupport.ARROW :
                        navigationIconSelector.getMarkedButtonId() + 1);
            }
        });
        initRadioContainers();
    }

    private void initRadioContainers() {
        navigationIconSelector = (RadioContainer) findComponentById(ResourceTable.Id_navigation_icon);
        navigationIconSelector.mark(2);
        navigationIconSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                materialSearchView.setNavigationIconSupport(i + 1);
            }
        });

        layoutManagerSelector = (RadioContainer) findComponentById(ResourceTable.Id_layout_manager);
        layoutManagerSelector.mark(0);
        layoutManagerSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                if (i == 0) {
                    DirectionalLayoutManager layoutManager = new DirectionalLayoutManager();
                    layoutManager.setOrientation(Component.VERTICAL);
                    materialSearchView.setAdapterLayoutManager(layoutManager);
                } else {
                    TableLayoutManager layoutManager = new TableLayoutManager();
                    layoutManager.setColumnCount(2);
                    layoutManager.setOrientation(Component.HORIZONTAL);
                    materialSearchView.setAdapterLayoutManager(layoutManager);
                }
            }
        });

        inputTypeSelector = (RadioContainer) findComponentById(ResourceTable.Id_font);
        inputTypeSelector.mark(0);
        inputTypeSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextTypeface(Font.DEFAULT);
                        break;
                    case 1:
                        materialSearchView.setTextTypeface(Font.DEFAULT_BOLD);
                        break;
                    case 2:
                        materialSearchView.setTextTypeface(Font.MONOSPACE);
                        break;
                    case 3:
                        materialSearchView.setTextTypeface(Font.SANS_SERIF);
                        break;
                    case 4:
                        materialSearchView.setTextTypeface(Font.SERIF);
                        break;
                    default:
                        break;
                }
            }
        });

        inputTypeSelector = (RadioContainer) findComponentById(ResourceTable.Id_input_type);
        inputTypeSelector.mark(0);
        inputTypeSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextInputType(InputAttribute.PATTERN_TEXT);
                        break;
                    case 1:
                        materialSearchView.setTextInputType(InputAttribute.PATTERN_NUMBER);
                        break;
                    case 2:
                        materialSearchView.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                        break;
                    default:
                        break;
                }
            }
        });

        imeOptionSelector = (RadioContainer) findComponentById(ResourceTable.Id_enter_option);
        imeOptionSelector.mark(1);
        imeOptionSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextImeOptions(InputAttribute.ENTER_KEY_TYPE_UNSPECIFIED);
                        break;
                    case 1:
                        materialSearchView.setTextImeOptions(InputAttribute.ENTER_KEY_TYPE_SEARCH);
                        break;
                    case 2:
                        materialSearchView.setTextImeOptions(InputAttribute.ENTER_KEY_TYPE_GO);
                        break;
                    case 3:
                        materialSearchView.setTextImeOptions(InputAttribute.ENTER_KEY_TYPE_SEND);
                        break;
                    default:
                        break;
                }
            }
        });
        hintSelector = (RadioContainer) findComponentById(ResourceTable.Id_hint);
        hintSelector.mark(1);
        hintSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextHint("");
                        break;
                    case 1:
                        materialSearchView.setTextHint("Search");
                        break;
                    case 2:
                        materialSearchView.setTextHint("Input something");
                        break;
                    default:
                        break;
                }
            }
        });

        textColorSelector = (RadioContainer) findComponentById(ResourceTable.Id_text_color);
        textColorSelector.mark(0);
        textColorSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextColor(getContext().getColor(ohos.global.systemres.ResourceTable.Color_id_color_text_primary));
                        break;
                    case 1:
                        materialSearchView.setTextColor(Color.RED.getValue());
                        break;
                    case 2:
                        materialSearchView.setTextColor(Color.GREEN.getValue());
                        break;
                    case 3:
                        materialSearchView.setTextColor(Color.BLUE.getValue());
                        break;
                    default:
                        break;
                }
            }
        });

        textSizeSelector = (RadioContainer) findComponentById(ResourceTable.Id_text_size);
        textSizeSelector.mark(1);
        textSizeSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextSize(14);
                        break;
                    case 1:
                        materialSearchView.setTextSize(16);
                        break;
                    case 2:
                        materialSearchView.setTextSize(18);
                        break;
                    default:
                        break;
                }
            }
        });

        hintColorSelector = (RadioContainer) findComponentById(ResourceTable.Id_hint_color);
        hintColorSelector.mark(0);
        hintColorSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setTextHintColor(getContext().getColor(ohos.global.systemres.ResourceTable.Color_id_color_text_primary));
                        break;
                    case 1:
                        materialSearchView.setTextHintColor(Color.YELLOW.getValue());
                        break;
                    case 2:
                        materialSearchView.setTextHintColor(Color.CYAN.getValue());
                        break;
                    case 3:
                        materialSearchView.setTextHintColor(Color.MAGENTA.getValue());
                    default:
                        break;
                }
            }
        });
        elevationSelector = (RadioContainer) findComponentById(ResourceTable.Id_elevation);
        elevationSelector.mark(0);
        elevationSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                if (i == 0) {
                    materialSearchView.setElevation(9);
                } else {
                    materialSearchView.setElevation(1);
                }
            }
        });

        radiusSelector = (RadioContainer) findComponentById(ResourceTable.Id_radius);
        radiusSelector.mark(1);
        radiusSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setBackgroundRadius(0);
                        break;
                    case 1:
                        materialSearchView.setBackgroundRadius(15);
                        break;
                    case 2:
                        materialSearchView.setBackgroundRadius(30);
                        break;
                    default:
                        break;
                }
            }
        });
        strokeColorSelector = (RadioContainer) findComponentById(ResourceTable.Id_stroke_color);
        strokeColorSelector.mark(2);
        strokeColorSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setBackgroundStrokeColor(Color.TRANSPARENT.getValue());
                        break;
                    case 1:
                        materialSearchView.setBackgroundStrokeColor(Color.GRAY.getValue());
                        break;
                    case 2:
                        materialSearchView.setBackgroundStrokeColor(0xff6200ee);
                        break;
                    case 3:
                        materialSearchView.setBackgroundStrokeColor(0xffff7f7f);
                        break;
                    default:
                        break;
                }
            }
        });
        strokeWidthSelector = (RadioContainer) findComponentById(ResourceTable.Id_stroke_width);
        strokeWidthSelector.mark(1);
        strokeWidthSelector.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        materialSearchView.setBackgroundStrokeWidth(0);
                        break;
                    case 1:
                        materialSearchView.setBackgroundStrokeWidth(AttrHelper.vp2px(1,getContext()));
                        break;
                    case 2:
                        materialSearchView.setBackgroundStrokeWidth(AttrHelper.vp2px(5,getContext()));
                        break;
                    case 3:
                        materialSearchView.setBackgroundStrokeWidth(AttrHelper.vp2px(10,getContext()));
                    default:
                        break;
                }
            }
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void showSearchResultDialog(String queryWord) {
        new ToastDialog(getContext()).setText("Found: " + queryWord + " in 10ms!!!").setDuration(5000).show();
    }


}
