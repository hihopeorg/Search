package com.lapism.search.internal;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.*;

public class SearchEditTextTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    @Test
    public void setClearFocusOnBackPressed() {
        SearchEditText searchEditText = new SearchEditText(context);
        searchEditText.setClearFocusOnBackPressed(true);
        assertEquals(true,searchEditText.isClearFocusOnBackPressed());
    }

    @Test
    public void isClearFocusOnBackPressed() {
        SearchEditText searchEditText = new SearchEditText(context);
        searchEditText.setClearFocusOnBackPressed(true);
        assertEquals(true,searchEditText.isClearFocusOnBackPressed());
    }
}