package com.lapism.search.util;

import com.lapism.search.ResourceTable;
import org.junit.Test;

import static com.lapism.search.internal.SearchEditTextTest.context;
import static org.junit.Assert.*;

public class UiUtilTest {

    @Test
    public void getVpAsPx() {
        assertEquals(3,UiUtil.getVpAsPx(ResourceTable.Float_search_divider,context));
    }

    @Test
    public void getFpAsPx() {
        assertEquals(144,UiUtil.getVpAsPx(ResourceTable.Float_search_icon_48,context));
    }
}