package com.lapism.search.sample;



import com.lapism.search.internal.SearchEditText;
import com.lapism.search.internal.SearchLayout;
import com.lapism.search.sample.util.EventHelper;
import com.lapism.search.widget.MaterialSearchView;
import com.lapism.search.widget.SimpleMeterialCardView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {


    private static Ability ability = EventHelper.startAbility(MainAbility.class);

    /**
     * 导航图标测试
     */
    @Test
    public void test01_navigationIconTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();

        RadioContainer navigationIcon = getRadioContainer(ResourceTable.Id_navigation_icon, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < navigationIcon.getChildCount(); position++) {
            Component menuButton = navigationIcon.getComponentAt(position);
            Assert.assertTrue("第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int iconType = materialSearchView.getNavigationIconSupport();
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position + 1);
        }
    }

    /**
     * 布局测试
     */
    @Test
    public void test02_layoutManagerTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer layoutManager = getRadioContainer(ResourceTable.Id_layout_manager, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < layoutManager.getChildCount(); position++) {
            Component menuButton = layoutManager.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ListContainer mRecyclerView = null;
            try {
                Field f = SearchLayout.class.getDeclaredField("mRecyclerView");
                f.setAccessible(true);
                mRecyclerView = (ListContainer) f.get(materialSearchView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到mRecyclerView");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }

            int iconType = -1;
            try {
                DirectionalLayoutManager layoutManager1 = (DirectionalLayoutManager) mRecyclerView.getLayoutManager();
                if (layoutManager1.getOrientation() == Component.VERTICAL) {
                    iconType = 0;
                }
            } catch (Exception e) {
                try {
                    TableLayoutManager layoutManager1 = (TableLayoutManager) mRecyclerView.getLayoutManager();
                    if (layoutManager1.getOrientation() == Component.HORIZONTAL && layoutManager1.getColumnCount() == 2) {
                        iconType = 1;
                    }
                } catch (Exception e1) {
                    iconType = -1;
                }
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 字体测试
     */
    @Test
    public void test03_fontTest() {

        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer font = getRadioContainer(ResourceTable.Id_font, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < font.getChildCount(); position++) {
            Component menuButton = font.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int iconType = -1;
            Font textTypeface = materialSearchView.getTextTypeface();
            if (textTypeface.equals(Font.DEFAULT)) {
                iconType = 0;
            } else if (textTypeface.equals(Font.DEFAULT_BOLD)) {
                iconType = 1;
            } else if (textTypeface.equals(Font.MONOSPACE)) {
                iconType = 2;
            } else if (textTypeface.equals(Font.SANS_SERIF)) {
                iconType = 3;
            } else if (textTypeface.equals(Font.SERIF)) {
                iconType = 4;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 输入类型测试
     */
    @Test
    public void test04_inputTypeTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer inputType = getRadioContainer(ResourceTable.Id_input_type, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < inputType.getChildCount(); position++) {
            Component menuButton = inputType.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int textInputType = materialSearchView.getTextInputType();
            int iconType = -1;
            if (textInputType == InputAttribute.PATTERN_TEXT) {
                iconType = 0;
            } else if (textInputType == InputAttribute.PATTERN_NUMBER) {
                iconType = 1;
            } else if (textInputType == InputAttribute.PATTERN_PASSWORD) {
                iconType = 2;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 输入选项测试
     */
    @Test
    public void test05_enterOptionTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();

        RadioContainer enterOption = getRadioContainer(ResourceTable.Id_enter_option, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < enterOption.getChildCount(); position++) {
            Component menuButton = enterOption.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int textImeOptions = materialSearchView.getTextImeOptions();
            int iconType = -1;
            if (textImeOptions == InputAttribute.ENTER_KEY_TYPE_UNSPECIFIED) {
                iconType = 0;
            } else if (textImeOptions == InputAttribute.ENTER_KEY_TYPE_SEARCH) {
                iconType = 1;
            } else if (textImeOptions == InputAttribute.ENTER_KEY_TYPE_GO) {
                iconType = 2;
            } else if (textImeOptions == InputAttribute.ENTER_KEY_TYPE_SEND) {
                iconType = 3;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }


    /**
     * 提示词测试
     */
    @Test
    public void test06_hintTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer hint = getRadioContainer(ResourceTable.Id_hint, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < hint.getChildCount(); position++) {
            Component menuButton = hint.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            String textHint = materialSearchView.getTextHint();
            int iconType = -1;
            if (textHint.equals("")) {
                iconType = 0;
            } else if (textHint.equals("Search")) {
                iconType = 1;
            } else if (textHint.equals("Input something")) {
                iconType = 2;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 文本颜色测试
     */
    @Test
    public void test07_textColorTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer textColor = getRadioContainer(ResourceTable.Id_text_color, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < textColor.getChildCount(); position++) {
            Component menuButton = textColor.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SearchEditText mSearchEditText = null;
            try {
                Field f = SearchLayout.class.getDeclaredField("mSearchEditText");
                f.setAccessible(true);
                mSearchEditText = (SearchEditText) f.get(materialSearchView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到mSearchEditText");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }
            Color textColour = mSearchEditText.getTextColor();
            int iconType = -1;
            if (textColour.getValue() == (ability.getContext().getColor(ohos.global.systemres.ResourceTable.Color_id_color_text_primary))) {
                iconType = 0;
            } else if (textColour.getValue() == (Color.RED.getValue())) {
                iconType = 1;
            } else if (textColour.getValue() == (Color.GREEN.getValue())) {
                iconType = 2;
            } else if (textColour.getValue() == (Color.BLUE.getValue())) {
                iconType = 3;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 字体大小测试
     */
    @Test
    public void test08_textSizeTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer textSize = getRadioContainer(ResourceTable.Id_text_size, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < textSize.getChildCount(); position++) {
            Component menuButton = textSize.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SearchEditText mSearchEditText = null;
            try {
                Field f = SearchLayout.class.getDeclaredField("mSearchEditText");
                f.setAccessible(true);
                mSearchEditText = (SearchEditText) f.get(materialSearchView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到mSearchEditText");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }

            int size = mSearchEditText.getTextSize(Text.TextSizeType.FP);
            int iconType = -1;
            if (size == 14) {
                iconType = 0;
            } else if (size == 16) {
                iconType = 1;
            } else if (size == 18) {
                iconType = 2;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 提示颜色测试
     */
    @Test
    public void test09_hintColorTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer hintColor = getRadioContainer(ResourceTable.Id_hint_color, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < hintColor.getChildCount(); position++) {
            Component menuButton = hintColor.getComponentAt(hintColor.getChildCount() - position - 1);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (hintColor.getChildCount() - position) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SearchEditText mSearchEditText = null;
            try {
                Field f = SearchLayout.class.getDeclaredField("mSearchEditText");
                f.setAccessible(true);
                mSearchEditText = (SearchEditText) f.get(materialSearchView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到mSearchEditText");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }

            Color textHintColor = mSearchEditText.getHintColor();
            int iconType = -1;
            if (textHintColor.getValue() == (ability.getContext().getColor(ohos.global.systemres.ResourceTable.Color_id_color_text_primary))) {
                iconType = 0;
            } else if (textHintColor.getValue() == (Color.YELLOW.getValue())) {
                iconType = 1;
            } else if (textHintColor.getValue() == (Color.CYAN.getValue())) {
                iconType = 2;
            } else if (textHintColor.getValue() == (Color.MAGENTA.getValue())) {
                iconType = 3;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(textHintColor.getValue() + ":" + (ability.getContext().getColor(ohos.global.systemres.ResourceTable.Color_id_color_text_primary)), iconType == hintColor.getChildCount() - position - 1);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (hintColor.getChildCount() - position) + "个iconType修改失败", iconType == hintColor.getChildCount() - position - 1);
        }
    }

    /**
     * 阴影测试
     */
    @Test
    public void test10_elevationTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer elevation = getRadioContainer(ResourceTable.Id_elevation, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < elevation.getChildCount(); position++) {
            Component menuButton = elevation.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            float viewElevation = materialSearchView.getElevation();
            int iconType = -1;
            if (viewElevation == 9) {
                iconType = 0;
            } else if (viewElevation == 1) {
                iconType = 1;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 圆角测试
     */
    @Test
    public void test11_radiusTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer radius = getRadioContainer(ResourceTable.Id_radius, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < radius.getChildCount(); position++) {
            Component menuButton = radius.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            float backgroundRadius = materialSearchView.getBackgroundRadius();
            int iconType = -1;
            if (backgroundRadius == 0) {
                iconType = 0;
            } else if (backgroundRadius == 15) {
                iconType = 1;
            } else if (backgroundRadius == 30) {
                iconType = 2;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 边框颜色测试
     */
    @Test
    public void test12_strokeColorTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer strokeColor = getRadioContainer(ResourceTable.Id_stroke_color, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < strokeColor.getChildCount(); position++) {
            Component menuButton = strokeColor.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SimpleMeterialCardView mMaterialCardView = null;
            try {
                Field f = SearchLayout.class.getDeclaredField("mMaterialCardView");
                f.setAccessible(true);
                mMaterialCardView = (SimpleMeterialCardView) f.get(materialSearchView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到mMaterialCardView");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }

            int strokeColour = -1;
            try {
                Field f = SimpleMeterialCardView.class.getDeclaredField("strokeColor");
                f.setAccessible(true);
                strokeColour = (int) f.get(mMaterialCardView);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException("没有找到strokeColor");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("非法访问");
            }

            int backgroundStrokeColor = strokeColour;
            int iconType = -1;
            if (backgroundStrokeColor == Color.TRANSPARENT.getValue()) {
                iconType = 0;
            } else if (backgroundStrokeColor == Color.GRAY.getValue()) {
                iconType = 1;
            } else if (backgroundStrokeColor == 0xff6200ee) {
                iconType = 2;
            } else if (backgroundStrokeColor == 0xffff7f7f) {
                iconType = 3;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }

    /**
     * 边框宽度测试
     */
    @Test
    public void test13_strokeWidthTest() {
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        RadioContainer strokeWidth = getRadioContainer(ResourceTable.Id_stroke_width, method);
        MaterialSearchView materialSearchView = getMaterialSearchView(method);

        for (int position = 0; position < strokeWidth.getChildCount(); position++) {
            Component menuButton = strokeWidth.getComponentAt(position);
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个menuButton获取失败", menuButton != null);
            AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, menuButton);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int backgroundStrokeWidth = materialSearchView.getBackgroundStrokeWidth();
            int iconType = -1;
            if (backgroundStrokeWidth == 0) {
                iconType = 0;
            } else if (backgroundStrokeWidth == AttrHelper.vp2px(1, ability.getContext())) {
                iconType = 1;
            } else if (backgroundStrokeWidth == AttrHelper.vp2px(5, ability.getContext())) {
                iconType = 2;
            } else if (backgroundStrokeWidth == AttrHelper.vp2px(10, ability.getContext())) {
                iconType = 3;
            } else {
                iconType = -1;
            }
            Assert.assertTrue(method.substring(0, method.length() - 4) + "第" + (position + 1) + "个iconType修改失败", iconType == position);
        }
    }
//    @Test
    public void test014_searchTest(){//todo 无法给输入法授权
        String method = Thread.currentThread().getStackTrace()[1].getMethodName();
        MaterialSearchView materialSearchView = (MaterialSearchView) ability.findComponentById(ResourceTable.Id_search1);
        stopThread(1000);
        exec("input tap 400 350");
//        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,materialSearchView);
        stopThread(2000);
        exec("input tap 400 350");

        stopThread(10000);
//        exec("input tap 700 2000");
//        stopThread(2000);
        exec("input tap 540 1650");
        stopThread(2000);
        exec("input tap 1000 2000");
        stopThread(2000);
        exec("input tap 1000 2000");

    }
    @Test
    public void test015_ReturnTest(){
        MaterialSearchView materialSearchView = (MaterialSearchView) ability.findComponentById(ResourceTable.Id_search1);
        stopThread(2000);
        boolean b = materialSearchView.hasFocus();
        exec("input tap 400 350");
        stopThread(2000);
        boolean b1 = materialSearchView.hasFocus();
        Image image =(Image) ability.findComponentById(com.lapism.search.ResourceTable.Id_search_image_view_navigation);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,image);
        stopThread(2000);
        boolean b2 = materialSearchView.hasFocus();
        Assert.assertTrue("返回失败",b1!=b2&&b2==b);
        stopThread(2000);
    }
    private void stopThread(int x){
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public MaterialSearchView getMaterialSearchView(String method) {
        MaterialSearchView materialSearchView = (MaterialSearchView) ability.findComponentById(ResourceTable.Id_search1);
        Assert.assertTrue(method.substring(0, method.length() - 4) + "未能获得搜索框", materialSearchView != null);
        return materialSearchView;
    }

    public RadioContainer getRadioContainer(int id, String method) {
        RadioContainer radioContainer = (RadioContainer) ability.findComponentById(id);
        Assert.assertTrue(method.substring(0, method.length() - 4) + " is null", radioContainer != null);
        return radioContainer;

    }

}