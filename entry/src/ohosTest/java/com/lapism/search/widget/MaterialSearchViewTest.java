package com.lapism.search.widget;


import org.junit.Test;

import java.lang.reflect.Field;

import static com.lapism.search.internal.SearchEditTextTest.context;
import static org.junit.Assert.assertEquals;

public class MaterialSearchViewTest {


    @Test
    public void addFocus() {
        MaterialSearchView materialSearchView = new MaterialSearchView(context);
        materialSearchView.addFocus();
        try {
            Field field = MaterialSearchView.class.getDeclaredField("mStrokeWidth");
            field.setAccessible(true);
            Object fieldValue = field.get(materialSearchView);
            assertEquals(0,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void removeFocus() {
        MaterialSearchView materialSearchView = new MaterialSearchView(context);
        materialSearchView.addFocus();
        materialSearchView.removeFocus();
        try {
            Field field = MaterialSearchView.class.getDeclaredField("mStrokeWidth");
            field.setAccessible(true);
            Object fieldValue = field.get(materialSearchView);
            assertEquals(0,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


}