package com.lapism.search.widget;

import org.junit.Test;

import java.lang.reflect.Field;

import static com.lapism.search.internal.SearchEditTextTest.context;
import static org.junit.Assert.*;

public class SimpleMeterialCardViewTest {

    @Test
    public void setCardBackgroundColor() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setCardBackgroundColor(7579);
        try {
            Field field = SimpleMeterialCardView.class.getDeclaredField("backgroundColor");
            field.setAccessible(true);
            Object fieldValue = field.get(simpleMeterialCardView);
            assertEquals(7579,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setElevation() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setElevation(7579);
        assertEquals(7579,simpleMeterialCardView.getElevation(),0.0);
    }

    @Test
    public void setCardElevaion() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setCardElevaion(7579);
        try {
            Field field = SimpleMeterialCardView.class.getDeclaredField("cardElevation");
            field.setAccessible(true);
            Object fieldValue = field.get(simpleMeterialCardView);
            assertEquals(7579,(float)fieldValue,0.0);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setMaxCardElevation() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setMaxCardElevation(7579);
        try {
            Field field = SimpleMeterialCardView.class.getDeclaredField("maxCardElevation");
            field.setAccessible(true);
            Object fieldValue = field.get(simpleMeterialCardView);
            assertEquals(7579,(float)fieldValue,0.0);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getElevation() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setElevation(7579);
        assertEquals(7579,simpleMeterialCardView.getElevation(),0.0);
    }

    @Test
    public void setRadius() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setRadius(1);
        assertEquals(1,simpleMeterialCardView.getRadius(),0.0);
    }

    @Test
    public void getRadius() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setRadius(1);
        assertEquals(1,simpleMeterialCardView.getRadius(),0.0);
    }

    @Test
    public void setStrokeColor() {
    }

    @Test
    public void setStrokeWidth() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setStrokeWidth(1);
        assertEquals(1,simpleMeterialCardView.getStrokeWidth(),0.0);
    }

    @Test
    public void getStrokeWidth() {
        SimpleMeterialCardView simpleMeterialCardView = new SimpleMeterialCardView(context);
        simpleMeterialCardView.setStrokeWidth(1);
        assertEquals(1,simpleMeterialCardView.getStrokeWidth(),0.0);
    }
}