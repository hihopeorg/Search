/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lapism.search.widget;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;
import ohos.app.Context;

public class SimpleMeterialCardView extends StackLayout {
    private ShapeElement strokeElement = new ShapeElement();
    private ShapeElement backgroundElement = new ShapeElement();
    private ShapeElement shadowElement = new ShapeElement();

    private float radius;
    private int backgroundColor;
    private float cardElevation;
    private float maxCardElevation;
    private int strokeColor;
    private int strokeWidth;
    private DrawTask strokeDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            strokeElement.setBounds(new Rect(0, 0, component.getWidth(), component.getHeight()));
            strokeElement.setShape(ShapeElement.RECTANGLE);
            strokeElement.setStroke(strokeWidth, RgbColor.fromArgbInt(strokeColor));
            strokeElement.setCornerRadius(radius);
            strokeElement.drawToCanvas(canvas);
        }
    };

    private DrawTask backgroundDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            backgroundElement.setBounds(new Rect(0, 0, component.getWidth(), component.getHeight()));
            backgroundElement.setShape(ShapeElement.RECTANGLE);
            backgroundElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
            backgroundElement.setCornerRadius(radius + strokeWidth / 2);
            backgroundElement.drawToCanvas(canvas);
        }
    };

    private DrawTask shadowDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            if (cardElevation < 1) {
                return;
            }
            Rect childPosition = SimpleMeterialCardView.this.getComponentPosition();
            childPosition.shrink((int) (-cardElevation), (int) (-cardElevation));
            shadowElement.setBounds(childPosition);
            shadowElement.setShape(ShapeElement.RECTANGLE);
            shadowElement.setStroke((int) cardElevation, RgbColor.fromArgbInt(0x0f000000));
            shadowElement.setCornerRadius((cardElevation + strokeWidth) / 2 + radius);
            shadowElement.drawToCanvas(canvas);
        }
    };

    public SimpleMeterialCardView(Context context) {
        super(context);
        init();
    }

    public SimpleMeterialCardView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public SimpleMeterialCardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        addDrawTask(backgroundDrawTask, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        addDrawTask(strokeDrawTask, DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
        if (getComponentParent() != null) {
            ((Component) getComponentParent()).addDrawTask(shadowDrawTask, DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
        } else {
            setBindStateChangedListener(new BindStateChangedListener() {
                @Override
                public void onComponentBoundToWindow(Component component) {
                    Component parent = ((Component) component.getComponentParent());
                    if (parent != null) {
                        parent.addDrawTask(shadowDrawTask, DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
                    }
                    component.setBindStateChangedListener(null);
                }

                @Override
                public void onComponentUnboundFromWindow(Component component) {

                }
            });
        }

    }

    public void setCardBackgroundColor(int color) {
        backgroundColor = color;
        invalidate();
    }

    public void setElevation(float elevation) {
        cardElevation = elevation;
        if (getComponentParent() != null) {
            ((Component) getComponentParent()).invalidate();
        }
    }

    public void setCardElevaion(float elevation) {
        cardElevation = elevation;
        if (getComponentParent() != null) {
            ((Component) getComponentParent()).invalidate();
        }
    }

    public void setMaxCardElevation(float elevation) {
        maxCardElevation = elevation;
        if (getComponentParent() != null) {
            ((Component) getComponentParent()).invalidate();
        }
    }

    public float getElevation() {
        return cardElevation;
    }

    public void setRadius(float radius) {
        this.radius = radius;
        invalidate();
        if (getComponentParent() != null) {
            ((Component) getComponentParent()).invalidate();
        }
    }

    public float getRadius() {
        return radius;
    }

    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        invalidate();
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
        invalidate();
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }
}
