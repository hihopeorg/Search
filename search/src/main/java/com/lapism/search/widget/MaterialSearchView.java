package com.lapism.search.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentTransition;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import com.lapism.search.ResourceTable;
import com.lapism.search.internal.SearchLayout;
import com.lapism.search.util.UiUtil;

import java.io.IOException;


public class MaterialSearchView extends SearchLayout /*implements CoordinatorLayout.AttachedBehavior*/ {
    // *********************************************************************************************
    private ComponentTransition mTransition = null;
    private int mStrokeWidth = 0;
    private float mRadius = 0f;
    private float mElevation = 0f;

    // *********************************************************************************************

    public MaterialSearchView(Context context) {
        this(context, null);
    }

    public MaterialSearchView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public MaterialSearchView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initMaterialSearchView(attrSet);
    }

    private void initMaterialSearchView(AttrSet attrSet) {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_search_view, this, true);
        init();
        setTransition();

        if (attrSet == null) {
            return;
        }

        if (attrSet.getAttr("search_navigationIconSupport").isPresent()) {
            setNavigationIconSupport(parseNavigationIconSupport(attrSet.getAttr("search_navigationIconSupport").get().getStringValue()));
        }

        if (attrSet.getAttr("search_navigationIcon").isPresent()) {
            setNavigationIconImageDrawable(UiUtil.getElement(attrSet.getAttr("search_navigationIcon").get().getIntegerValue(), getContext()));
        }

        if (attrSet.getAttr("search_clearIcon").isPresent()) {
            setClearIconImageDrawable(UiUtil.getElement(attrSet.getAttr("search_clearIcon").get().getIntegerValue(),
                    getContext()));
        } else {
            setClearIconImageDrawable(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_search_ic_outline_clear_24));
        }

        if (attrSet.getAttr("search_micIcon").isPresent()) {
            setMicIconImageDrawable(UiUtil.getElement(attrSet.getAttr("search_micIcon").get().getIntegerValue(),
                    getContext()));
        } else {
            setMicIconImageDrawable(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_search_ic_outline_mic_none_24));
        }

        if (attrSet.getAttr("search_dividerColor").isPresent()) {
            setDividerColor(getContext().getColor(attrSet.getAttr("search_dividerColor").get().getIntegerValue()));
        }

        if (attrSet.getAttr("search_shadowColor").isPresent()) {
            setShadowColor(getContext().getColor(attrSet.getAttr("search_shadowColor").get().getIntegerValue()));
        } else {
            setShadowColor(getContext().getColor(ResourceTable.Color_search_shadow));
        }

        if (attrSet.getAttr("search_textHint").isPresent()) {
            setTextHint(attrSet.getAttr("search_textHint").get().getStringValue());
        }

        if (attrSet.getAttr("search_strokeColor").isPresent()) {//ref
            setBackgroundStrokeColor(getContext().getColor(attrSet.getAttr("search_strokeColor").get().getIntegerValue()));
        }

        if (attrSet.getAttr("search_strokeWidth").isPresent()) {//ref
            setBackgroundStrokeWidth(getContext().getColor(attrSet.getAttr("search_strokeWidth").get().getIntegerValue()));
        }

        if (attrSet.getAttr("search_transitionDuration").isPresent()) {
            setTransitionDuration(attrSet.getAttr("search_transitionDuration").get().getIntegerValue());
        } else {
            try {
                setTransitionDuration(getContext().getResourceManager().getElement(ResourceTable.Integer_search_animation_duration).getInteger());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }

        if (attrSet.getAttr("search_radius").isPresent()) {
            setBackgroundRadius(attrSet.getAttr("search_radius").get().getDimensionValue());
        } else {
            setBackgroundRadius(UiUtil.getVpAsPx(ResourceTable.Float_search_radius, getContext()));
        }

        if (attrSet.getAttr("search_elevation").isPresent()) {
            setElevation(attrSet.getAttr("search_elevation").get().getDimensionValue());
        } else {
            setElevation(UiUtil.getVpAsPx(ResourceTable.Float_search_elevation, getContext()));
        }

        if (attrSet.getAttr("input_enter_key_type").isPresent()) {
            setTextImeOptions(attrSet.getAttr("input_enter_key_type").get().getIntegerValue());
        }

        if (attrSet.getAttr("text_input_type").isPresent()) {
            setTextInputType(attrSet.getAttr("text_input_type").get().getIntegerValue());
        }
    }

    // *********************************************************************************************
    @Override
    public void addFocus() {
        if (mOnFocusChangeListener != null) {
            mOnFocusChangeListener.onFocusChange(true);
        }
        showKeyboard();

        mStrokeWidth = getBackgroundStrokeWidth();
        mRadius = getBackgroundRadius();
        mElevation = getElevation();

        setBackgroundStrokeWidth(UiUtil.getVpAsPx(ResourceTable.Float_search_stroke_width_focus, getContext()));
        setBackgroundRadius(UiUtil.getVpAsPx(ResourceTable.Float_search_radius_focus, getContext()));
        setElevation(UiUtil.getVpAsPx(ResourceTable.Float_search_elevation_focus, getContext()));

        int left = UiUtil.getVpAsPx(ResourceTable.Float_search_dp_16, getContext());
        DirectionalLayout.LayoutConfig params = (DirectionalLayout.LayoutConfig) mSearchEditText.getLayoutConfig();
        params.setMargins(left, 0, 0, 0);
        mSearchEditText.setLayoutConfig(params);

        setMarginsMode(Margins.FOCUS);
        setLayoutHeight(UiUtil.getVpAsPx(ResourceTable.Float_search_layout_height_focus, getContext()));

        mViewShadow.setVisibility(Component.VISIBLE);

        mViewDivider.setVisibility(VISIBLE);
        mViewAnim.setVisibility(VISIBLE);
        mRecyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void removeFocus() {
        if (mOnFocusChangeListener != null) {
            mOnFocusChangeListener.onFocusChange(false);
        }
        hideKeyboard();

        ComponentContainer.LayoutConfig params = mSearchEditText.getLayoutConfig();
        params.setMargins(0, 0, 0, 0);
        mSearchEditText.setLayoutConfig(params);

        setBackgroundStrokeWidth(mStrokeWidth);
        setBackgroundRadius(mRadius);
        setElevation(mElevation);

        setLayoutHeight(UiUtil.getVpAsPx(ResourceTable.Float_search_layout_height, getContext()));
        setMarginsMode(Margins.NO_FOCUS);

        mViewShadow.setVisibility(HIDE);

        mRecyclerView.setVisibility(HIDE);
        mViewAnim.setVisibility(HIDE);
        mViewDivider.setVisibility(HIDE);
    }

    public void setTransitionDuration(long duration) {
        if (mTransition != null) {
            mTransition.setTransitionTypeDuration(ComponentTransition.CHANGING, (int) duration);
        }
        setComponentTransition(mTransition);
    }

    private void setTransition() {
        mTransition = new ComponentTransition();
        mTransition.addTransitionType(ComponentTransition.CHANGING);
    }
}
