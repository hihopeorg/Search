package com.lapism.search.internal;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import com.lapism.search.ResourceTable;
import com.lapism.search.util.UiUtil;
import com.lapism.search.widget.SimpleMeterialCardView;

import java.lang.reflect.Method;


public abstract class SearchLayout extends StackLayout implements Component.ClickedListener {
    public SearchLayout(Context context) {
        this(context, null, null);
    }

    public SearchLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SearchLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    // *********************************************************************************************
    // Better way than enum class :-)
    public interface NavigationIconSupport {
        public static final int NONE = 0;
        public static final int MENU = 1;
        public static final int ARROW = 2;
        public static final int SEARCH = 3;
    }

    public interface Margins {
        public static final int NO_FOCUS = 4;
        public static final int FOCUS = 5;
    }

    // *********************************************************************************************
    private Image mImageViewMic = null;
    protected ListContainer mRecyclerView = null;
    private SimpleMeterialCardView mMaterialCardView = null;
    protected SearchEditText mSearchEditText = null;
    protected Component mViewShadow = null;
    protected Component mViewDivider = null;
    protected Component mViewAnim = null;
    protected OnFocusChangeListener mOnFocusChangeListener = null;

    private DirectionalLayout mLinearLayout = null;
    private Image mImageViewNavigation = null;
    private Image mImageViewClear = null;
    private OnQueryTextListener mOnQueryTextListener = null;
    private OnNavigationClickListener mOnNavigationClickListener = null;
    private OnMicClickListener mOnMicClickListener = null;
    private OnClearClickListener mOnClearClickListener = null;

    private int navigationIconSupport;
    private int margins;

    // *********************************************************************************************

    public final int parseNavigationIconSupport(String navigationIconSupportStr) {
        switch (navigationIconSupportStr) {
            case "NONE":
                return NavigationIconSupport.NONE;
            case "MENU":
                return NavigationIconSupport.MENU;
            case "ARROW":
                return NavigationIconSupport.ARROW;
            case "SEARCH":
                return NavigationIconSupport.SEARCH;
            default:
                return NavigationIconSupport.NONE;
        }
    }

    public final int getNavigationIconSupport() {
        return this.navigationIconSupport;
    }

    public final void setNavigationIconSupport(int navigationIconSupport) {
        this.navigationIconSupport = navigationIconSupport;
        switch (navigationIconSupport) {
            case NavigationIconSupport.NONE:
                this.setNavigationIconImageDrawable(null);
                break;
            case NavigationIconSupport.MENU:
                this.setNavigationIconImageDrawable(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_search_ic_outline_menu_24));
                break;
            case NavigationIconSupport.ARROW:
                this.setNavigationIconImageDrawable(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_search_ic_outline_arrow_back_24));
                break;
            case NavigationIconSupport.SEARCH:
                this.setNavigationIconImageDrawable(ElementScatter.getInstance(getContext()).parse(ResourceTable.Graphic_search_ic_outline_search_24));
            default:
                this.setNavigationIconImageDrawable(null);
                break;
        }

    }

    public final int getMarginsMode() {
        return margins;
    }

    public final void setMarginsMode(int margins) {
        this.margins = margins;

        int left;
        int top;
        int right;
        int bottom;
        LayoutConfig params = (LayoutConfig) (mMaterialCardView.getLayoutConfig());
        switch (margins) {
            case Margins.NO_FOCUS:

                left = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_left_right, getContext());
                top = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_top_bottom, getContext());
                right = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_left_right, getContext());
                bottom = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_top_bottom, getContext());
                if (params != null) {
                    params.width = LayoutConfig.MATCH_PARENT;
                    params.height = LayoutConfig.MATCH_CONTENT;
                    params.setMargins(left, top, right, bottom);
                }

                mMaterialCardView.setLayoutConfig(params);
                break;
            case Margins.FOCUS:
                left = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_focus, getContext());
                top = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_focus, getContext());
                right = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_focus, getContext());
                bottom = UiUtil.getVpAsPx(ResourceTable.Float_search_margins_focus, getContext());
                if (params != null) {
                    params.width = LayoutConfig.MATCH_PARENT;
                    params.height = LayoutConfig.MATCH_PARENT;
                    params.setMargins(left, top, right, bottom);
                }

                mMaterialCardView.setLayoutConfig(params);
        }
    }

    // *********************************************************************************************
    protected abstract void addFocus();

    protected abstract void removeFocus();

    // *********************************************************************************************
    protected void init() {
        mLinearLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_search_linear_layout);

        mImageViewNavigation = (Image) findComponentById(ResourceTable.Id_search_image_view_navigation);
        mImageViewNavigation.setClickedListener(this);

        mImageViewMic = (Image) findComponentById(ResourceTable.Id_search_image_view_mic);
        mImageViewMic.setClickedListener(this);

        mImageViewClear = (Image) findComponentById(ResourceTable.Id_search_image_view_clear);
        mImageViewClear.setVisibility(Component.HIDE);
        mImageViewClear.setClickedListener(this);

        mSearchEditText = (SearchEditText) findComponentById(ResourceTable.Id_search_search_edit_text);
        mSearchEditText.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                SearchLayout.this.onTextChanged(s);
            }
        });

        mSearchEditText.setEditorActionListener(new Text.EditorActionListener() {
            @Override
            public boolean onTextEditorAction(int i) {
                onSubmitQuery();
                return true;
            }
        });

        mSearchEditText.setFocusChangedListener(
                new FocusChangedListener() {
                    @Override
                    public void onFocusChange(Component component, boolean hasFocus) {
                        if (hasFocus) {
                            addFocus();
                        } else {
                            removeFocus();
                        }
                    }
                }
        );

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_search_recycler_view);
        mRecyclerView.setVisibility(Component.HIDE);
        mRecyclerView.setLayoutManager(new DirectionalLayoutManager());
        mRecyclerView.setTouchEventListener(new TouchEventListener() {
            private int lastAction;

            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                // onScrollStateChanged to SCROLL_STATE_DRAGGING
                if (lastAction == TouchEvent.PRIMARY_POINT_DOWN && touchEvent.getAction() == TouchEvent.POINT_MOVE) {
                    hideKeyboard();
                }
                lastAction = touchEvent.getAction();
                return false;
            }
        });

        mViewShadow = (Component) findComponentById(ResourceTable.Id_search_view_shadow);
        mViewShadow.setVisibility(Component.HIDE);


        mViewDivider = (Component) findComponentById(ResourceTable.Id_search_view_divider);

        mViewDivider.setVisibility(Component.HIDE);


        mViewAnim = (Component) findComponentById(ResourceTable.Id_search_view_anim);
        mViewAnim.setVisibility(Component.HIDE);


        mMaterialCardView = (SimpleMeterialCardView) findComponentById(ResourceTable.Id_search_material_card_view);
        setMarginsMode(Margins.NO_FOCUS);


        setClickable(true);
        setFocusable(FOCUS_ENABLE);
        setTouchFocusable(true);
    }

    // *********************************************************************************************
    public void setNavigationIconVisibility(int visibility) {
        mImageViewNavigation.setVisibility(visibility);
    }

    public void setNavigationIconImageResource(int resId) {
        mImageViewNavigation.setImageElement(UiUtil.getElement(resId, getContext()));
    }

    public void setNavigationIconImageDrawable(Element drawable) {
        mImageViewNavigation.setImageElement(drawable);
    }

    public void setNavigationIconContentDescription(CharSequence contentDescription) {
        mImageViewNavigation.setComponentDescription(contentDescription);
    }

    // *********************************************************************************************
    public void setMicIconImageResource(int resId) {
        mImageViewMic.setImageElement(UiUtil.getElement(resId, getContext()));
    }

    public void setMicIconImageDrawable(Element drawable) {
        mImageViewMic.setImageElement(drawable);
    }

    public void setMicIconContentDescription(CharSequence contentDescription) {
        mImageViewMic.setComponentDescription(contentDescription);
    }

    // *********************************************************************************************
    public void setClearIconImageResource(int resId) {
        mImageViewClear.setImageElement(UiUtil.getElement(resId, getContext()));
    }

    public void setClearIconImageDrawable(Element drawable) {
        mImageViewClear.setImageElement(drawable);
    }

    public void setClearIconContentDescription(CharSequence contentDescription) {
        mImageViewClear.setComponentDescription(contentDescription);
    }

    // *********************************************************************************************
    public void setAdapterLayoutManager(LayoutManager layout) {
        mRecyclerView.setLayoutManager(layout);
    }

    // only when height == match_parent
    public void setAdapterHasFixedSize(boolean hasFixedSize) {
        mRecyclerView.setAutoLayout(hasFixedSize, 1);
    }

    public void setAdapter(BaseItemProvider adapter) {
        mRecyclerView.setItemProvider(adapter);
    }

    public BaseItemProvider getAdapter() {
        return mRecyclerView.getItemProvider();
    }

    // *********************************************************************************************

    public void setTextTypeface(Font font) {
        mSearchEditText.setFont(font);
    }

    public Font getTextTypeface() {
        return mSearchEditText.getFont();
    }
    public void setTextInputType(int type) {
        mSearchEditText.setTextInputType(type);
    }

    public int getTextInputType() {
        return mSearchEditText.getTextInputType();
    }

    public void setTextImeOptions(int imeOptions) {
        mSearchEditText.setInputMethodOption(imeOptions);
    }

    public int getTextImeOptions() {
        return mSearchEditText.getInputMethodOption();
    }

    public void setTextQuery(String query, boolean submit) {
        mSearchEditText.setText(query);
        mSearchEditText.setSelected(true);
        if (submit && !TextTool.isNullOrEmpty(query)) {
            onSubmitQuery();
        }
    }

    public String getTextQuery() {
        return mSearchEditText.getText();
    }

    public void setTextHint(String hint) {
        mSearchEditText.setHint(hint);
    }

    public String getTextHint() {
        return mSearchEditText.getHint();
    }

    public void setTextColor(int color) {
        mSearchEditText.setTextColor(new Color(color));
    }

    public void setTextSize(int size) {
        mSearchEditText.setTextSize(size, Text.TextSizeType.FP);
    }

    public void setTextGravity(int gravity) {
        mSearchEditText.setTextAlignment(gravity);
    }

    public void setTextHint(int resid) {
        mSearchEditText.setHint(getContext().getString(resid));
    }

    public void setTextHintColor(int color) {
        mSearchEditText.setHintColor(new Color(color));
    }

    public void setClearFocusOnBackPressed(boolean clearFocusOnBackPressed) {
        mSearchEditText.setClearFocusOnBackPressed(clearFocusOnBackPressed);
    }

    // *********************************************************************************************

    public void setBackgroundColor(int color) {
        mMaterialCardView.setCardBackgroundColor(color);
    }

    public void setElevation(float elevation) {
        mMaterialCardView.setCardElevaion(elevation);
        mMaterialCardView.setMaxCardElevation(elevation);
    }

    public float getElevation() {
        return mMaterialCardView.getElevation();
    }

    public void setBackgroundRadius(float radius) {
        mMaterialCardView.setRadius(radius);
    }

    public float getBackgroundRadius() {
        return mMaterialCardView.getRadius();
    }

    public void setBackgroundStrokeColor(int strokeColor) {
        mMaterialCardView.setStrokeColor(strokeColor);
    }

    public void setBackgroundStrokeWidth(int strokeWidth) {
        mMaterialCardView.setStrokeWidth(strokeWidth);
    }

    public int getBackgroundStrokeWidth() {
        return mMaterialCardView.getStrokeWidth();
    }

    // *********************************************************************************************
    public void setDividerColor(int color) {
        ShapeElement background = new ShapeElement();
        background.setRgbColor(RgbColor.fromArgbInt(color));
        mViewDivider.setBackground(background);
    }

    public void setShadowColor(int color) {
        ShapeElement background = new ShapeElement();
        background.setRgbColor(RgbColor.fromArgbInt(color));
        mViewShadow.setBackground(background);
    }

    // *********************************************************************************************
    public void setOnFocusChangeListener(OnFocusChangeListener listener) {
        mOnFocusChangeListener = listener;
    }

    public void setOnQueryTextListener(OnQueryTextListener listener) {
        mOnQueryTextListener = listener;
    }

    public void setOnNavigationClickListener(OnNavigationClickListener listener) {
        mOnNavigationClickListener = listener;
    }

    public void setOnMicClickListener(OnMicClickListener listener) {
        mOnMicClickListener = listener;
    }

    public void setOnClearClickListener(OnClearClickListener listener) {
        mOnClearClickListener = listener;
    }

    // *********************************************************************************************
    public void showKeyboard() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput", int.class, boolean.class);
            startInput.invoke(object, 1, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method stopInput = inputClass.getMethod("stopInput", int.class);
            stopInput.invoke(object, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // *********************************************************************************************
    protected void setLayoutHeight(int height) {
        ComponentContainer.LayoutConfig params = mLinearLayout.getLayoutConfig();
        if (params != null) {
            params.height = height;
            params.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        }
        mLinearLayout.setLayoutConfig(params);
    }

    // *********************************************************************************************
    private void onTextChanged(String newText) {
        if (!TextTool.isNullOrEmpty(newText)) {
            mImageViewMic.setVisibility(Component.HIDE);
            mImageViewClear.setVisibility(Component.VISIBLE);
        } else {
            mImageViewMic.setVisibility(Component.VISIBLE);
            mImageViewClear.setVisibility(Component.HIDE);
        }

        if (mOnQueryTextListener != null) {
            mOnQueryTextListener.onQueryTextChange(newText);
        }
    }

    private void onSubmitQuery() {
        String query = mSearchEditText.getText();
        if (query != null && query.trim().length() > 0) {
            if (mOnQueryTextListener == null || !mOnQueryTextListener.onQueryTextSubmit(query.toString())) {
                mSearchEditText.setText(query);
            }
        }
    }

    // *********************************************************************************************

    @Override
    public boolean requestFocus() {
        if (!isFocusable()) {
            return false;
        } else {
            return mSearchEditText.requestFocus();
        }
    }

    @Override
    public void clearFocus() {
        super.clearFocus();
        mSearchEditText.clearFocus();
    }

    public void onClick(Component view) {
        if (view == mImageViewNavigation) {
            if (mOnNavigationClickListener != null) {
                mOnNavigationClickListener.onNavigationClick(mSearchEditText.hasFocus());
            }
        } else if (view == mImageViewMic) {
            if (mOnMicClickListener != null) {
                mOnMicClickListener.onMicClick();
            }
        } else if (view == mImageViewClear) {
            if (!mSearchEditText.getText().isEmpty()) {
                mSearchEditText.setText("");
            }
            if (mOnClearClickListener != null) {
                mOnClearClickListener.onClearClick();
            }
        }
    }

    // *********************************************************************************************
    public interface OnFocusChangeListener {
        void onFocusChange(boolean hasFocus);
    }

    public interface OnQueryTextListener {

        boolean onQueryTextChange(String newText);

        boolean onQueryTextSubmit(String query);
    }

    public interface OnNavigationClickListener {

        void onNavigationClick(boolean hasFocus);
    }

    public interface OnMicClickListener {

        void onMicClick();
    }

    public interface OnClearClickListener {

        void onClearClick();
    }

}
