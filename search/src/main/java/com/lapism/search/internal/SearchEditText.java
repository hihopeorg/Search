package com.lapism.search.internal;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;

/**
 * @hide
 */
public class SearchEditText extends TextField {
    private boolean clearFocusOnBackPressed = false;

    public SearchEditText(Context context) {
        super(context);
        init();
    }

    public SearchEditText(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public SearchEditText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    public void setClearFocusOnBackPressed(boolean clearFocusOnBackPressed) {
        this.clearFocusOnBackPressed = clearFocusOnBackPressed;
    }

    private void init() {
        setKeyEventListener(new KeyEventListener() {
            @Override
            public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEY_BACK && !keyEvent.isKeyDown() && true) {
                    if (hasFocus()) {
                        clearFocus();
                        return true;
                    }
                }
                return false;

            }
        });
    }

    public boolean isClearFocusOnBackPressed() {
        return clearFocusOnBackPressed;
    }

    @Override
    public void clearFocus() {
        super.clearFocus();
        if (!TextTool.isNullOrEmpty(getText())) {
            setText("");
        }
    }
}
