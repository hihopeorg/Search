/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lapism.search.util;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Theme;
import ohos.global.systemres.ResourceTable;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class UiUtil {
    /**
     * treat the resource as vp, then convert to px
     *
     * @param resId   resource id
     * @param context context
     * @return px value
     */
    public static int getVpAsPx(int resId, Context context) {
        int result = 0;
        try {
            result = AttrHelper.vp2px(context.getResourceManager().getElement(resId).getFloat(), context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * treat the resource as fp, then convert to px
     *
     * @param resId   resource id
     * @param context context
     * @return px value
     */
    public static int getFpAsPx(int resId, Context context) {
        int result = 0;
        try {
            result = AttrHelper.fp2px(context.getResourceManager().getElement(resId).getFloat(), context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Element getElement(int resId, Context context) {
        Element result = null;
        try {
            ElementScatter.getInstance(context).parse(resId);
        } catch (IllegalArgumentException e) {
            PixelMap pixelMap = null;
            try {
                ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(resId), null);
                pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
                result = new PixelMapElement(pixelMap);
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (NotExistException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static int getToolbarIconColor(Context context) {
        int color = 0;
        try {
            Theme theme = context.getTheme();
            if (theme == null) {
                theme = context.getResourceManager().getTheme(ResourceTable.Theme_theme_light);
            }
            if (theme != null) {
                color = theme.getColorValue("attr_color_toolbar_icon");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return color;
    }
}
