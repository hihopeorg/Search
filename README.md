# Search

**本项目是基于开源项目Search进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/lapism/search ）追踪到原项目版本**

#### 项目介绍

- 项目名称：Search自定义搜索框UI库
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一套自定义搜索框控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/lapism/search
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示

<img src="gif/Search1.gif"/>

<img src="gif/Search2.gif"/>

<img src="gif/Search3.gif"/>

#### 安装教程

方法1.

1. 下载search三方库har包search.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'search', ext: 'har')
	……
}
```

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.lapism.ohos:search:1.0.1'
}
```

#### 使用说明

### 代码
```java

        materialSearchView = (MaterialSearchView) findComponentById(ResourceTable.Id_search1);
        materialSearchView.setAdapterLayoutManager(layoutManager);
        materialSearchView.setAdapter(adapter);

        materialSearchView.setNavigationIconSupport(SearchLayout.NavigationIconSupport.SEARCH);
        materialSearchView.setOnNavigationClickListener(new SearchLayout.OnNavigationClickListener() {
            @Override
            public void onNavigationClick(boolean hasFocus) {
                if (materialSearchView.hasFocus()) {
                    materialSearchView.clearFocus();
                } else {
                    materialSearchView.requestFocus();
                }
            }
        });

        materialSearchView.setTextHint("Search");
        materialSearchView.setOnQueryTextListener(new SearchLayout.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                showSearchResultDialog(query);
                return true;
            }
        });

        materialSearchView.setOnMicClickListener(new SearchLayout.OnMicClickListener() {
            @Override
            public void onMicClick() {
                // call System's ASR service
            }
        });

        materialSearchView.setBackgroundRadius(15);
        materialSearchView.setBackgroundStrokeWidth(AttrHelper.vp2px(1, getContext()));
        materialSearchView.setBackgroundStrokeColor(strokeColor);
        materialSearchView.setOnFocusChangeListener(new SearchLayout.OnFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                materialSearchView.setNavigationIconSupport(hasFocus ? SearchLayout.NavigationIconSupport.ARROW :
                        SearchLayout.NavigationIconSupport.SEARCH);
            }
        });
```

### 布局
参考如下示例

```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">

    <com.lapism.search.widget.MaterialSearchView
        ohos:id="$+id:search1"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:input_enter_key_type="enter_key_type_search"
        ohos:text_input_type="pattern_text"
        />
        <DirectionalLayout
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:orientation="horizontal"
        ohos:top_margin="15vp">
        ... ...
        </DirectionalLayout>
</DirectionalLayout>
```

### XML 自定义属性
```xml
        属性名 search_navigationIconSupport 类型 enum
            可用值 "none"
                   "menu"
                   "arrow"
                   "search"
        属性名 search_navigationIcon 类型 reference
        属性名 search_clearIcon 类型 reference
        属性名 search_micIcon 类型 reference
        属性名 search_textHint 类型 string
        属性名 search_strokeColor 类型 reference
        属性名 search_strokeWidth 类型 reference
        属性名 search_dividerColor 类型 reference
        属性名 search_shadowColor 类型 reference
        属性名 search_transitionDuration 类型 integer
        属性名 search_radius 类型 integer
        属性名 elevation 类型 integer
        ohos:input_enter_key_type
        ohos:text_input_type
```

#### 版本迭代

- v1.0.1

#### 版权和许可信息

```
Apache License 2.0

```
